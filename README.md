# Belajar Kafka #

* Instalasi Kafka di Ubuntu menggunakan [tutorial dari DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-apache-kafka-on-ubuntu-20-04)

* Menjalankan database development dan ui-kafka

    ```
    docker-compose up
    ```

* Database bisa diakses di port `5432`

    ```
    psql -h 127.0.0.1 -U training -d dbvirtualaccount
    ```

* Tampilan untuk membuat dan melihat message di kafka bisa diakses di [http://localhost:10001](http://localhost:10001)