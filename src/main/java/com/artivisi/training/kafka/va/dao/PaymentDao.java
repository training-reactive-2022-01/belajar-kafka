package com.artivisi.training.kafka.va.dao;

import com.artivisi.training.kafka.va.entity.Payment;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaymentDao extends PagingAndSortingRepository<Payment, String> {
    
}
