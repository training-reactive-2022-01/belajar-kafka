package com.artivisi.training.kafka.va.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CreateVaRequest {
    private String nomor;
    private String nama;
    private String keterangan;
    private BigDecimal nilai;
    private LocalDateTime waktuDibuat = LocalDateTime.now();
    private LocalDate jatuhTempo = LocalDate.now().plusMonths(3); 
}
