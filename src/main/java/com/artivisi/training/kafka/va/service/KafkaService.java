package com.artivisi.training.kafka.va.service;

import com.artivisi.training.kafka.va.dao.VirtualAccountDao;
import com.artivisi.training.kafka.va.dto.CreateVaRequest;
import com.artivisi.training.kafka.va.dto.CreateVaResponse;
import com.artivisi.training.kafka.va.dto.PaymentResponse;
import com.artivisi.training.kafka.va.entity.Payment;
import com.artivisi.training.kafka.va.entity.VirtualAccount;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class KafkaService {

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired private VirtualAccountDao virtualAccountDao;

    @Value("${va.topic.create.response}") 
    private String topicCreateVaResponse;

    @Value("${va.topic.payment.response}") 
    private String topicPaymentResponse;

    @KafkaListener(topics = "${va.topic.create.request}")
    public void createVirtualAccountRequest(String msg){
        try {
            CreateVaRequest request = 
                objectMapper.readValue(msg, CreateVaRequest.class);

            log.debug("Terima request create va : {}", request.toString());

            VirtualAccount va = new VirtualAccount();
            BeanUtils.copyProperties(request, va);

            CreateVaResponse response = new CreateVaResponse();

            try {
                virtualAccountDao.save(va);
                response.setSukses(true);
                response.setNomor(request.getNomor());
                response.setNama(request.getNama());
            } catch (DataIntegrityViolationException err) {
                log.warn("Nomor akun {} sudah terpakai", request.getNomor());
                response.setSukses(false);
                response.setError("Nomor akun "+request.getNomor()+" sudah terpakai");
                response.setNomor(request.getNomor());
                response.setNama(request.getNama());
            }

            String strResponse = objectMapper.writeValueAsString(response);
            kafkaTemplate.send(topicCreateVaResponse, strResponse);
            log.debug("Kirim response create va : {}", strResponse);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void createPaymentResponse(Payment payment){
        PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.setNomor(payment.getVirtualAccount().getNomor());
        paymentResponse.setNama(payment.getVirtualAccount().getNama());
        paymentResponse.setReferensi(payment.getReferensi());
        paymentResponse.setWaktuPembayaran(payment.getWaktuPembayaran());

        try {
            log.info("Mengirim notifikasi pembayaran {}", paymentResponse);
            String strPaymentResponse = objectMapper.writeValueAsString(paymentResponse);
            kafkaTemplate.send(topicPaymentResponse, strPaymentResponse);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }
}
