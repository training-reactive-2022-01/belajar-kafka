package com.artivisi.training.kafka.va.dto;

import lombok.Data;

@Data
public class CreateVaResponse {
    private Boolean sukses;
    private String nomor;
    private String nama;
    private String error;
}
