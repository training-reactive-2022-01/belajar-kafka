package com.artivisi.training.kafka.va.dao;

import com.artivisi.training.kafka.va.entity.VirtualAccount;
import com.artivisi.training.kafka.va.entity.VirtualAccountStatus;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface VirtualAccountDao extends PagingAndSortingRepository<VirtualAccount, String> {

    VirtualAccount findByNomor(String account);

    VirtualAccount findByNomorAndStatus(String account, VirtualAccountStatus active);
    
}
