package com.artivisi.training.kafka.va.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data
public class VirtualAccount {

    @Id @GeneratedValue(generator = "uuid2") 
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;
    private String nomor;
    private String nama;
    private String keterangan;
    private LocalDateTime waktuDibuat = LocalDateTime.now();
    private LocalDate jatuhTempo = LocalDate.now().plusMonths(3);
    private BigDecimal nilai;

    @Enumerated(EnumType.STRING)
    private VirtualAccountStatus status = VirtualAccountStatus.ACTIVE;
}
