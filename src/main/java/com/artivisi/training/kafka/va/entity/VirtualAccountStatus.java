package com.artivisi.training.kafka.va.entity;

public enum VirtualAccountStatus {
    ACTIVE, EXPIRED, PAID
}
