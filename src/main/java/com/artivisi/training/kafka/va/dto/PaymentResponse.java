package com.artivisi.training.kafka.va.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PaymentResponse {
    private String nomor;
    private String nama;
    private BigDecimal nilai;
    private LocalDateTime waktuPembayaran;
    private String referensi;
}
