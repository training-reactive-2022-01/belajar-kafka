package com.artivisi.training.kafka.va.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity @Data
public class Payment {

    @Id @GeneratedValue(generator = "uuid2") 
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @ManyToOne @JoinColumn(name = "id_virtual_account")
    private VirtualAccount virtualAccount;

    private String referensi;
    private LocalDateTime waktuPembayaran = LocalDateTime.now();
    
}
