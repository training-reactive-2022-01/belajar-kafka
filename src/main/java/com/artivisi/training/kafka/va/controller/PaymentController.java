package com.artivisi.training.kafka.va.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.artivisi.training.kafka.va.dao.PaymentDao;
import com.artivisi.training.kafka.va.dao.VirtualAccountDao;
import com.artivisi.training.kafka.va.entity.Payment;
import com.artivisi.training.kafka.va.entity.VirtualAccount;
import com.artivisi.training.kafka.va.entity.VirtualAccountStatus;
import com.artivisi.training.kafka.va.service.KafkaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/api")
public class PaymentController {

    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PaymentDao paymentDao;

    @Autowired private KafkaService kafkaService;

    @Value("${instance.name}")
    private String instanceName;

    @GetMapping("/instance/info")
    public Map<String, String> info(HttpServletRequest request){
        Map<String, String> hasil = new HashMap<>();
        hasil.put("instance", instanceName);
        hasil.put("ip", request.getLocalAddr());
        hasil.put("port", String.valueOf(request.getLocalPort()));
        return hasil;
    }

    @PostMapping("/payment/{account}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void payment(@PathVariable String account){
        VirtualAccount va = virtualAccountDao.findByNomorAndStatus(account, VirtualAccountStatus.ACTIVE);
        if(va != null) {
            Payment payment = new Payment();
            payment.setVirtualAccount(va);
            payment.setReferensi(UUID.randomUUID().toString());
            
            va.setStatus(VirtualAccountStatus.PAID);
            
            paymentDao.save(payment);
            virtualAccountDao.save(va);

            kafkaService.createPaymentResponse(payment);
        }
    }
}
