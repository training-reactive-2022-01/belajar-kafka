create table virtual_account (
    id varchar(36), 
    nomor varchar(100) not null,
    nama varchar(100) not null,
    keterangan varchar(255),
    nilai decimal(19,2) not null,
    waktu_dibuat timestamp not null,
    jatuh_tempo date not null,
    status varchar(100) not null,
    primary key (id), 
    unique (nomor)
);

create table payment (
    id varchar(36),
    id_virtual_account varchar(36) not null,
    referensi varchar(50) not null,
    waktu_pembayaran timestamp not null,
    primary key (id),
    foreign key (id_virtual_account) references virtual_account(id)
);